<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Lead;
use App\Models\OpenLeads;
use App\Models\Customer;
use App\Models\Sphere;
use App\Models\SphereAttr;
use App\Models\SphereAttrOptions;


use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Sentinel;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;




class TestController extends BaseController
{


//    public function index(User $user, Lead $lead)
//    {
//        $lead = $lead ->all();
//
//        $user = $user ->all();
//
//        return view('test.index', compact('user' ,'lead'  ) );
//    }
//
//
//
//    public function getData(User $user, $id)
//    {
//        $user = $user->find($id);
//
//        return response()->json([
//           'user' =>  $user
//        ]);
//    }

    public function index(  )
    {



        $user = Lead::all();
        $Sphere = Sphere::all();
        $SphereAttr = SphereAttr::all();
        $SphereAttrOptions = SphereAttrOptions::all();



//        $user = Lead::where('abc', 'a')->orWhere('abc', 'b')->orWhere('abc', 'c')->get();


//        $user = Lead::where('id', '=', 7)->get();
//        $user = Lead::whereIn('id', ['7', '8'])->get();

        $OpenLeads = OpenLeads::get()->toArray();


        $list = OpenLeads::with('lead')->get()->toArray();


        $tel = Lead::with('obtainedBy')->get()->toArray();


        return view('test.index', compact('user', 'list','tel', 'OpenLeads', 'Sphere','SphereAttr', 'SphereAttrOptions'  ) );
    }



    public function getData(Lead $user, $id)
    {
        $user = $user->find($id);

        return response()->json([
            'user' =>  $user
        ]);
    }


}
